
import sys
import subprocess
import frida
import time

def print_help():
    print("""
Usage:
    chm-avd [action] [action options]
Actions:
    help                                : Print usage.
    list                                : List all android virtual devices.
    create [root|noroot] [device_name]  : Create new android virtual devices root/no root.
    run [device_name]                   : Run device.
    shell [adb_device_id]               : Run adb shell.
    turnoff [adb_device_id]             : Shutdown avd device.
    delete [device_name]                : Delete android virtual device.""")

def get_int(question):
    while (True):
        i = input(question)
        try:
            i = int(i)
        except:
            print("[-] Error value. Input is not a number")
            continue
        return i - 1

def selectRunningDevice():
    devices = frida.enumerate_devices()
    time.sleep(1)

    phones = []
    counter = 1
    for d in devices:
        if (d.type == "usb"):
            print("({counter}) Name: {name} - Id: {id}".format(counter=counter,id=d.id,name=d.name))
            counter += 1
            phones.append(d)

    if (len(phones) == 0):
        print("[-] No device found. Exit")
        sys.exit(0)
    
    if (len(phones) == 1):
        return phones[0]

    while True:
        try:
            choose = int(input("\n Select Device > "))
            choose -= 1
            if not (0 <= choose < len(phones)):
                print ("[-] {} is not in list".format(choose+1))
            else:
                break
        except ValueError:
            print("[-] Error value. Input is not a number")

    return phones[choose]

def list_avd_device():
    subprocess.call("avdmanager list avd", shell=True)

def create_root_device(device_name = "rooted_device", device_image = "system-images;android-30;google_apis;x86_64"):
    subprocess.call(["avdmanager", "create", "avd", "--name", device_name, "-k", device_image, "-d", "pixel_4_xl"], shell=True)

def create_avd_device():
    if (len(sys.argv) == 2):
        create_root_device()
    elif(len(sys.argv) == 3):
        if (sys.argv[2] == "root"):
            create_root_device()
        if (sys.argv[2] == "noroot"):
            create_root_device(device_name = "no_root_device", device_image = "system-images;android-30;google_apis_playstore;x86_64")
    else:
        if (sys.argv[2] == "root"):
            create_root_device(device_name = sys.argv[3])
        if (sys.argv[2] == "noroot"):
            create_root_device(device_name = sys.argv[3], device_image = "system-images;android-30;google_apis_playstore;x86_64")

def run_avd():
    if (len(sys.argv) == 2):
        device_list = subprocess.Popen(["emulator","-list-avds"], shell=True,
                                        stdout=subprocess.PIPE, stderr=subprocess.STDOUT).stdout.read().decode("utf-8").splitlines()
        if (len(device_list) == 0):
            exit(0)
        if (len(device_list) == 1):
            device = device_list[0]
        else:
            print("List of available devices:")
            for i in range(len(device_list)):
                print(str(i + 1) + ". " + device_list[i])
            device = device_list[get_int("Select Device > ")]
        
        subprocess.call(["emulator", "-avd", device], shell=True)
    else:
        subprocess.call(["emulator", "-avd", sys.argv[2]], shell=True)

def adb_shell():
    if (len(sys.argv) == 2):
        device = selectRunningDevice()
        print(device.id)
        subprocess.call(["adb", "-s", device.id, "shell"], shell=True)
    else:
        subprocess.call(["adb", "-s", sys.argv[2], "shell"], shell=True)

def turnoff_device():
    if (len(sys.argv) == 2):
        device = selectRunningDevice()
        print(device.id)
        subprocess.call(["adb", "-s", device.id, "shell", "reboot", "-p"], shell=True)
    else:
        subprocess.call(["adb", "-s", sys.argv[2], "shell", "reboot", "-p"], shell=True)

def delete_device():
    if (len(sys.argv) == 2):
        device_list = subprocess.Popen(["emulator","-list-avds"], shell=True,
                                        stdout=subprocess.PIPE, stderr=subprocess.STDOUT).stdout.read().decode("utf-8").splitlines()
        if (len(device_list) == 0):
            exit(0)
        if (len(device_list) == 1):
            device = device_list[0]
        else:
            print("List of available devices:")
            for i in range(len(device_list)):
                print(str(i + 1) + ". " + device_list[i])
            device = device_list[get_int("Select Device > ")]
        
        subprocess.call(["avdmanager", "delete", "avd", "-n", device], shell=True)
    else:
        subprocess.call(["avdmanager", "delete", "avd", "-n", sys.argv[2]], shell=True)
n = len(sys.argv)

if (n < 2 or sys.argv[1] == "help"):
    print_help()
elif (sys.argv[1] == "list"):
    list_avd_device()
elif (sys.argv[1] == "create"):
    create_avd_device()
elif (sys.argv[1] == "run"):
    run_avd()
elif (sys.argv[1] == "shell"):
    adb_shell()
elif (sys.argv[1] == "turnoff"):
    turnoff_device()
elif (sys.argv[1] == "delete"):
    delete_device()
